<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index()
    {
        $posts = Post::with(['comments','user','category'])->paginate(15);
        return response()->json([
            'data'=>$posts
        ],200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'content' => 'required',
            'privacy' => 'required',
            'category_id' => 'required',
        ]);
        $user_id= auth()->user()->id;

        $post = new Post;
        $post->title = $request->title;
        $post->content = $request->content;
        $post->status = $request->status;
        $post->privacy = $request->privacy;
        $slug = str_slug($request->title, '-');
        $post->slug = $slug;
        $post->category_id = $request->category_id;
        $post->user_id = $user_id;

        $category = Category::find($request->category_id);
        if($category){
            $category->posts()->save($post);
            return response()->json([
                "message" => "Post created",
                "data" => $post
            ], 201);
        }else{
            return response()->json([
                "message" => "Post not Found",
            ], 404);
        }
        }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Post $id)
    {
        if (empty($post)) {
            return response()->json([
                'data'=> $id
            ],200);
          } else {
            return response()->json([
              "message" => "Post not found"
            ], 404);
          }

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $id)
    {
        $request->validate([
            'title' => 'required',
            'content' => 'required',
            'privacy' => 'required',
            'category_id' => 'required',
        ]);
        $post_id = $id->id;
        $user_id= auth()->user()->id;
        if (Post::where('id', $post_id)->exists()) {

            $post = Post::find($post_id);

            $post->title = $request->title;
            $post->content = $request->content;
            $post->privacy = $request->privacy;
            $slug = str_slug($request->title, '-');
            $post->slug = $slug;
            $post->category_id = $request->category_id;
            $post->user_id = $user_id;

            $category = Category::find($request->category_id);
            if($category){
                $category->posts()->save($post);
                return response()->json([
                    "message" => "Post updated successfully",
                    "data" => $post
                ], 201);
            }
            else {
                return response()->json([
                "message" => "Post not found"
                ], 404);
            }

        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $id)
    {
        $post_id = $id->id;

        $post = Post::find($post_id);
        $post->delete();
        return response()->json([
            "message" => "Post deleted Successfully"
        ], 202);

    }
    // public function fetchPostFromCategory($cat_id)
    // {
    //     $posts = Post::where('category_id', $cat_id)->get();
    //     return response()->json([
    //         'data'=>$posts
    //     ],200);
    // }
}
