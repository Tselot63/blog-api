<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        return response()->json([
            'data'=>$categories
        ],200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);
        $category = new Category;
        $category->name = $request->name;
        $slug = str_slug($request->name, '-');
        $category->slug = $slug;
        $category->save();

        return response()->json([
            "message" => "Category created",
            "data" => $category
        ], 201);
        }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $id)
    {
        if (empty($category)) {
            return response()->json([
                'data'=> $id
            ],200);
          } else {
            return response()->json([
              "message" => "Category not found"
            ], 404);
          }

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $id)
    {
        $request->validate([
            'name' => 'required',
        ]);
        $cat_id = $id->id;
        if (Category::where('id', $cat_id)->exists()) {
            $category = Category::find($id)->first();
            $category->name = $request->name;
            $slug = str_slug($request->name, '-');
            $category->slug = $slug;
            $category->save();
            return response()->json([
                      "message" => "Category updated successfully",
                      "data" => $category
                    ], 200);
        }
        else {
            return response()->json([
              "message" => "Category not found"
            ], 404);
          }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $id)
    {
        $cat_id = $id->id;

            $category = Category::find($cat_id);
            $category->delete();
            return response()->json([
              "message" => "Category deleted Successfully"
            ], 202);

    }
}
