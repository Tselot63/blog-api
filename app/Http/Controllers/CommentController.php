<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Post;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comment = Comment::with(['post','user'])->get();
        return response()->json([
            'data'=>$comment
        ],200);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'content' => 'required',
            'post_id' => 'required',
        ]);
        $user_id= auth()->user()->id;
        $comment = new Comment;
        $comment->content = $request->content;
        $comment->post_id = $request->post_id;
        $comment->user_id = $user_id;

        $post = Post::find($request->post_id);
        if($post){
            $post->comments()->save($comment);

            return response()->json([
                "message" => "Comment created",
                "data" => $comment
            ], 201);
        }
        return response()->json([
            "message" => "Post does not exist",
        ], 404);
        }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function show(Comment $comment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Comment $comment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $id)
    {
        $comment_id = $id->id;
        $comment = Comment::find($comment_id);
        if($comment){
            $comment->delete();
            return response()->json([
                "message" => "Comment deleted Successfully"
            ], 202);
        }
        else{
            return response()->json([
                "message" => "No comment found"
            ], 404);

        }

    }
}
