<?php

namespace Database\Factories;

use App\Models\Post;
use Illuminate\Database\Eloquent\Factories\Factory;

class PostFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Post::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $title = $this->faker->name;
        return [
            'user_id' => function(){
                return \App\Models\User::inRandomOrder()->first()->id;
            },
            'category_id' => function(){
                return \App\Models\Category::inRandomOrder()->first()->id;
            },
            'title' => $title,
            'content' => $this->faker->text,
            'slug' => str_slug($title),
            'privacy' => function (){
                return rand(0,1) ? 'Private' : 'Public';
            },
            'status' => function (){
                return rand(0,1) ? 'Saved' : 'Posted';
            },


        ];
    }
}
