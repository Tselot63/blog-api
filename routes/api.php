<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['middleware' => ['cors', 'json.response']], function () {

    //Authentication **public**
    Route::post('/login', 'Auth\ApiAuthController@login')->name('login.api');
    Route::post('/register','Auth\ApiAuthController@register')->name('register.api');

    //show posts **public**
    Route::get('post/{id}', 'PostController@show');
    Route::get('post', 'PostController@index');

    //show comments **public**
    Route::get('comment', 'CommentController@index');


    Route::middleware('auth:api')->group(function () {
        Route::get('/user', function (Request $request) {
            return $request->user();
        });


        Route::post('/logout', 'Auth\ApiAuthController@logout')->name('logout.api');
        Route::post('/logout', 'Auth\ApiAuthController@logout')->name('logout.api');


        //category routes
        Route::get('category', 'CategoryController@index');
        Route::get('category/{id}', 'CategoryController@show');
        Route::post('category', 'CategoryController@store');
        Route::put('category/{id}', 'CategoryController@update');
        Route::delete('category/{id}','CategoryController@destroy');


        //post routes
        Route::post('post', 'PostController@store');
        Route::put('post/{id}', 'PostController@update');
        Route::delete('post/{id}','PostController@destroy');
        // Route::get('category/{cat_id}/post', 'PostController@fetchPostFromCategory');



        //comment routes
        // Route::get('comment/{id}', 'CommentController@show');
        Route::post('comment', 'CommentController@store');
        // Route::put('comment/{id}', 'CommentController@update');
        Route::delete('comment/{id}','CommentController@destroy');
    });
});


